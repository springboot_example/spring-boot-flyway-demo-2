package cn.weeego.heiji.control;

import cn.weeego.heiji.base.User;
import io.swagger.annotations.ApiOperation;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/hello")
public class HelloController {

    @ApiOperation(value = "hello", notes = "")
    @RequestMapping(value = {"/world"}, method = RequestMethod.GET)
    public String getUserList(User user, Model model) {
        model.addAttribute("reqyestuser", user);
        System.out.println(model.asMap().get("zuaauser"));
        return model.toString();
    }


}