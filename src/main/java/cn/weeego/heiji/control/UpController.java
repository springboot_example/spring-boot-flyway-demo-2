package cn.weeego.heiji.control;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/hello")
public class UpController {

    @ApiOperation(value = "up", notes = "")
    @RequestMapping(value = {"/img"}, method = RequestMethod.GET)
    public String getUserList() {
        return "!asd";
    }


}