package cn.weeego.heiji.config;

import cn.weeego.heiji.base.User;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalModelControllerAdvice {

    @ModelAttribute("zuaauser")
    public User newUser(HttpServletRequest request) {


        String userName = request.getHeader("x-custom-name");
        User user = new User();
        if (userName == null) {
            userName = "zuaa_d";
        }
        user.setName(userName);
        System.out.println("============取头信息");
        return user;
    }

}

