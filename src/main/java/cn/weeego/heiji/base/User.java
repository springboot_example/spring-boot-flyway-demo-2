package cn.weeego.heiji.base;

import lombok.Data;

@Data
public class User {
    String name;
    String psw;
    String email;
    String photo;
}
