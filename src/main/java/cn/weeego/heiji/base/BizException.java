package cn.weeego.heiji.base;


public class BizException extends Exception {

    public BizException(String message) {
        super(message);
    }

}
